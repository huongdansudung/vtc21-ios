//
//  Auth.swift
//  VScanSGCo.op
//
//  Created by Mac on 17/05/2021.
//

import Foundation
import ObjectMapper

class Auth: Mappable {
 
    var center: String?
    var coopBranch: String?
    var id: Int?
    var idCus: Int?
    var idCus_new: Int?
    var message: String?
    var name: String?
    var refreshToken: String?
    var role: String?
    var accessToken: String?
    
    required init?(map: Map) {

    }
    
    func mapping(map: Map) {
        center <- map["center"]
        coopBranch <- map["coopBranch"]
        id <- map["id"]
        idCus <- map["idCus"]
        idCus_new <- map["idCus_new"]
        message <- map["message"]
        name <- map["name"]
        refreshToken <- map["refreshToken"]
        role <- map["role"]
        accessToken <- map["token"]
    }
}
