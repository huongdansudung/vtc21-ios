//
//  batchResponse.swift
//  VScanSGCo.op
//
//  Created by Mac on 17/05/2021.
//

import Foundation
import ObjectMapper

class BatchResponse: Mappable {
    var numberPhoto: Int?
    var batch: Int?
    var message: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        numberPhoto <- map["numberPhoto"]
        batch <- map["batch"]
        message <- map["Info"]
    }
}
