//
//  PhotoMutilResponse.swift
//  VScanSGCo.op
//
//  Created by Mac on 21/05/2021.
//

import Foundation
import ObjectMapper

class PhotoMutilResponse: Mappable {
    var page: String?
    var photoMutils: [PhotoMutil]?
    var totalPhoto: Int?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        page <- map["page"]
        photoMutils <- map["results"]
        totalPhoto <- map["tonganh"]
    }
}
