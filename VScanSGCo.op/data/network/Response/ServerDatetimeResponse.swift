//
//  ServerDatetimeResponse.swift
//  VScanSGCo.op
//
//  Created by Mac on 17/05/2021.
//

import Foundation
import ObjectMapper

class ServerDatetimeResponse: Mappable {
    var datetime: String?
    var message: String?
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        datetime <- map["datetime"]
        message <- map["message"]
    }
}
