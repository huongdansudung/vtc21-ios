//
//  UploadBatch.swift
//  VScanSGCo.op
//
//  Created by Mac on 27/05/2021.
//


import Foundation
import ObjectMapper

class UploadBatch: Mappable {
    var message: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        message <- map["message"]
    }
}
