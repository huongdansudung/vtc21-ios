//
//  ViewStatusResponse.swift
//  VScanSGCo.op
//
//  Created by Mac on 21/05/2021.
//
import Foundation
import ObjectMapper

class ViewStatusResponse: Mappable {
    var bills: [Bill]?

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        bills <- map["results"]
    }
}
