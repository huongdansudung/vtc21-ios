//
//  DetailViewController.swift
//  VScanSGCo.op
//
//  Created by Mac on 09/06/2021.
//
import UIKit

final class DetailViewController: UIViewController {

    private var detailTitle: String!

    class func make(detailTitle: String) -> UIViewController {
        let viewController = UIStoryboard(name: "DetailViewController", bundle: nil)
            .instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        viewController.detailTitle = detailTitle
        return viewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = detailTitle
    }
}
