//
//  SelectOptionViewController.swift
//  VScanSGCo.op
//
//  Created by Mac on 04/05/2021.
//

import UIKit

class SelectOptionViewController: UIViewController {
    var nameCoop: String!
    var centerCoop: String!

    @IBOutlet weak var labelNameCoop: UILabel!
    @IBOutlet weak var labelCenterCoop: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonUtils.countTime()
        labelNameCoop.text = nameCoop
        labelCenterCoop.text = centerCoop
        createFolder(titleFolder: "sgcimage")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getServerDateTime()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @IBAction func btnTakePhoto(_ sender: Any) {
        CommonUtils.timeOut = 3600
        if MGConnection.isConnectedToInternet() {
            DispatchQueue.main.asyncDeduped(target: self, after: 0.5, execute: { [self] in
                self.navigationToSelectBill()
            })
        }else {
            Toast.show(message: "Không tìm thấy server, Vui lòng kiểm tra lại internet!", controller: self)
        }
    }
    
    @IBAction func btnStatistical(_ sender: Any) {
        CommonUtils.timeOut = 3600
        if MGConnection.isConnectedToInternet() {
            DispatchQueue.main.asyncDeduped(target: self, after: 0.5, execute: { [self] in
                self.navigationToStatistical()
            })
        }else {
            Toast.show(message: "Không tìm thấy server, Vui lòng kiểm tra lại internet!", controller: self)
        }
    }

    
    func createFolder(titleFolder: String) {
        let manager = FileManager.default
        guard let url = manager.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return
        }
        let newUrl = url.appendingPathComponent(titleFolder)
        do {
            try manager.createDirectory(at: newUrl, withIntermediateDirectories: true, attributes: [:])
        }catch {
            print(error.localizedDescription)
        }
    }
    
    private func getServerDateTime() {
        MGConnection.request(APIRouter.getServerDatetime, ServerDatetimeResponse.self,completion: {(result, err) in
            guard err == nil else {
                Toast.show(message: (err?.mErrorMessage)!, controller: self)
                return
            }
            if result?.message! == "ok" {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let dateFormatterLocal = DateFormatter()
                dateFormatterLocal.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let serverTime = Int(dateFormatter.date(from: result?.datetime as! String)!.timeIntervalSince1970)
                let currentTime = Int(dateFormatterLocal.date(from: dateFormatterLocal.string(from: Date()))!.timeIntervalSince1970)
                CommonUtils.timeDifference = Double(currentTime - serverTime)
                let datetimeArray = (result?.datetime as! String).components(separatedBy: " ")
                if !CommonUtils.isDialogTimeOut {
                    let dateArray = datetimeArray[0].components(separatedBy: "-")
                    DispatchQueue.main.async {
                        self.removeAllOverlays()
                        self.checkDatetime(yearServer: Int(dateArray[0])!,
                                      monthServer: Int(dateArray[1])!,
                                      dayServer: Int(dateArray[2])!)
                        CommonUtils.isDialogTimeOut = true
                    }
                }
                
            }else {
                Toast.show(message: "Error time server", controller: self)
            }
        })
    }
    
    private func checkDatetime(yearServer: Int, monthServer: Int, dayServer: Int) {
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        let year =  components.year
        let month = components.month
        let day = components.day
        if year != yearServer || month != monthServer || day != dayServer {
            let alert = UIAlertController(title: "Cảnh báo!",
                                          message: "Thời gian của máy không chính xác: \(day!)/\(month!)/\(year!)",
                                          preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Dismiss",style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func navigationToSelectBill() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let selectBillScreen = sb.instantiateViewController(identifier: "SelectBills") as! SelectBillViewController
        self.navigationController?.pushViewController(selectBillScreen, animated: true)
    }
    
    private func navigationToStatistical() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let statisticalScreen = sb.instantiateViewController(identifier: "statistical") as! StatisticalViewController
        self.navigationController?.pushViewController(statisticalScreen, animated: true)
    }
}
