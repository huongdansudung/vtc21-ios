//
//  StatisticalViewController.swift
//  VScanSGCo.op
//
//  Created by Mac on 06/05/2021.
//

import UIKit
import MonthYearPicker

class StatisticalViewController: UIViewController {
    
    @IBOutlet weak var statisticalTableView: UITableView!
    @IBOutlet weak var txtFieldDate: UITextField!
    @IBOutlet weak var lblSumPhoto: UILabel!
    var months = ["Tháng 1","Tháng 2","Tháng 3","Tháng 4","Tháng 5","Tháng 6","Tháng 7","Tháng 8","Tháng 9","Tháng 10","Tháng 11","Tháng 12"]
    var years = ["2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027","2028","2029","2030","2031","2032",]
    let monthAndYearPicker = UIPickerView()
    let toolbar = UIToolbar()
    var statisticalBills: [Bill] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Thống kê"
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        monthAndYearPicker.dataSource = self
        monthAndYearPicker.delegate = self
        statisticalTableView.dataSource = self
        statisticalTableView.delegate = self
        txtFieldDate.inputView = monthAndYearPicker
        toolbar.sizeToFit()
        let btnDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonPressed))
        toolbar.items = [btnDone]
        txtFieldDate.inputAccessoryView = toolbar
        txtFieldDate.becomeFirstResponder()
        showDefaultValuePickView()
       // CommonUtils.navigationController = self.navigationController?
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
    }
    
    @objc func doneButtonPressed() {
        let month = months[monthAndYearPicker.selectedRow(inComponent: 0)]
        let year = years[monthAndYearPicker.selectedRow(inComponent: 1)]
        txtFieldDate.text = "\(month) Năm: \(year)"
        self.view.endEditing(true)
        self.loadStatistical(date: year.substring(from: 2) + String(format: "%02d", (month.substring(from: 6) as NSString).integerValue))
    }
    
    private func loadStatistical(date: String) {
        MGConnection.request(APIRouter.viewStatusMonth(date: date), ViewStatusResponse.self, completion: {(result, err) in
            guard err == nil else {
                DispatchQueue.main.sync {
                    Toast.show(message: (err?.mErrorMessage)!, controller: self)
                }
                return
            }
            self.statisticalBills = (result?.bills!)!
            DispatchQueue.main.async {
                self.statisticalTableView.reloadData()
                self.lblSumPhoto.text = "Tổng ảnh \(self.sumTotalPhoto(statisticalBills: self.statisticalBills))"
            }
        })
    }
    
    private func sumTotalPhoto(statisticalBills: [Bill]) -> Int {
        var countPhoto = 0
        for bill in statisticalBills {
            countPhoto += Int(bill.totalPhotoString)!
        }
        return countPhoto
    }
    
    private func showDefaultValuePickView() {
        let date = Date()
        let calendar = Calendar.current
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        for value in months {
            if String(value) == "Tháng " + String(month){
                print()
                monthAndYearPicker.selectRow(months.firstIndex(of: value)!, inComponent: 0, animated: true)
                
            }
        }
        for value in years {
            if String(value) == String(year) {
                monthAndYearPicker.selectRow(years.firstIndex(of: value)!, inComponent: 1, animated: true)
            }
        }
    }
 }

extension StatisticalViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
        
            return months.count
        }else {
     
            return months.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return months[row]
        }else {
            return years[row]
        }
    }
}

extension StatisticalViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statisticalBills.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as! StatisticalTableViewCell
        cell.lblTotalPhoto.text = String(statisticalBills[indexPath.row].totalPhotoString)
        cell.lblNumbericalOrder.text = String(indexPath.row)
        cell.lblTypeBill.text = statisticalBills[indexPath.row].nameBill
        return cell
    }
}

extension String {
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }

    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return String(self[fromIndex...])
    }

    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return String(self[..<toIndex])
    }

    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return String(self[startIndex..<endIndex])
    }
}
