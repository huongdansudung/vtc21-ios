//
//  CommonUtils.swift
//  VScanSGCo.op
//
//  Created by Mac on 12/05/2021.
//

import Foundation
import GTAlertCollection

class CommonUtils {
    static var timeOut = 3600
    static var timer = Timer()
    static var year: Int = 0
    static var month: Int = 0
    static var day: Int = 0
    static var timeDifference = 0.0
    static var isDialogTimeOut = false
    static let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL
    static let src = directory!.appendingPathComponent("sgcimage")
    static var navigationController: UINavigationController? = nil
    static var isWaitingShow = false
    
    
    static func getServerDatetime(simpleDateFormat: String) -> String {
        let dateFormatterLocal = DateFormatter()
        dateFormatterLocal.dateFormat = "yyyy-MM-dd HH:mm:ss"
        var currentTime = dateFormatterLocal.date(from: dateFormatterLocal.string(from: Date()))!.timeIntervalSince1970
        currentTime = currentTime - timeDifference
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd-HH-mm-ss"
        let string = dateFormat.string(from: Date())
        let timeArray  = string.components(separatedBy: "-")

        year = Int(timeArray[0])!
        month = Int(timeArray[1])!
        day = Int(timeArray[2])!
        switch simpleDateFormat {
        
        case "yyyyMMdd":
            return String(format: "%04d", Int(timeArray[0])!) +
                String(format: "%02d", Int(timeArray[1])!) +
                String(format: "%02d", Int(timeArray[2])!)
            
        case "yyyyMMddHHmmss":
            return String(format: "%04d",  Int(timeArray[0])!) +
                String(format: "%02d", Int(timeArray[1])!) +
                String(format: "%02d", Int(timeArray[2])!) +
                String(format: "%02d", Int(timeArray[3])!) +
                String(format: "%02d", Int(timeArray[4])!) +
                String(format: "%02d", Int(timeArray[5])!)
            
        case "yyyyMMdd_HHmmss":
            return String(format: "%04d", Int(timeArray[0])!) +
                String(format: "%02d", Int(timeArray[1])!) +
                String(format: "%02d", Int(timeArray[2])!) + "_" +
                String(format: "%02d", Int(timeArray[3])!) +
                String(format: "%02d", Int(timeArray[4])!) +
                String(format: "%02d", Int(timeArray[5])!)
            
        case "yyyy/MM/dd":
            return String(format: "%04d", timeArray[0]) + "/" +
                String(format: "%02d", timeArray[1]) + "/" +
                String(format: "%02d", timeArray[2])
            
        default:
            return "simple date formar wrong"
                        
        }
    }
    
    static func showProgress(title:String, message: String){
        GTAlertCollection.shared.presentActivityAlert(withTitle: title, message: message, activityIndicatorColor: .blue, showLargeIndicator: false, presentationCompletion: {_ in
            isWaitingShow = true
        })
        
    }
    
    static func dismissAlert() {
        GTAlertCollection.shared.dismissAlert(completion: {
            isWaitingShow = false
        })
    }
    
    static func countTime() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(counter), userInfo: nil, repeats: true)
    }
    
    @objc static func counter() {
        CommonUtils.timeOut -= 1
        print(CommonUtils.timeOut)
        if CommonUtils.timeOut == 0 {
            CommonUtils.timeOut = 3600
            CommonUtils.timer.invalidate()
            navigationController?.popToRootViewController(animated: true)
            Toast.show(message: "Kết thúc phiên làm việc qua 30 phút!", controller: navigationController!)
        }
    }
}
